package com.gaoyao.notice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String channelName = intent.getStringExtra("channelName");
        Intent i = new Intent(context, AlarmService.class);
        i.putExtra("channelName", channelName);
        context.startService(i);
    }
}