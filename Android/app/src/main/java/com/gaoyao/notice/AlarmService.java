package com.gaoyao.notice;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.Timer;

public class AlarmService extends Service {
    public String channelName;
    private NotificationManager notificationManager;
    private final int Time = 1000 * 60 * 5;//周期时间

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();
        // Init Global Notification Manager
        this.notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channel = new NotificationChannel("Notice", "Notice", NotificationManager.IMPORTANCE_HIGH);
            this.notificationManager.createNotificationChannel(channel);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Check is start loop
        NoticeApplication application = (NoticeApplication) this.getApplication();
        if (!application.getLoopStatus()) {
            stopSelf();
            return START_NOT_STICKY; // Not start
        }
        // Get channel name
        String channelName = intent.getStringExtra("channelName");
        if (channelName == null || channelName.length() == 0) {
            Toast.makeText(getApplicationContext(), "Please check the channel name.", Toast.LENGTH_SHORT).show();
            stopSelf();
            return START_NOT_STICKY;
        }
        // Sent notice list request
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
        // Date curDate = new Date(System.currentTimeMillis());
        // showNote("通知时间", formatter.format(curDate));
        try {
            URL url = new URL("http://此处改为Server地址/api/get_notice/" + channelName);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(6000);
            conn.setReadTimeout(6000);
            conn.setRequestMethod("GET");
            InputStream is = conn.getInputStream();
            String returnData = readStream(is);
            JSONObject result = new JSONObject(returnData);
            if (conn.getResponseCode() == 200) {
                Log.i(String.valueOf(0), returnData);
                if ((Integer) result.get("status") == 1) {
                    // 读取成功
                    JSONArray notice = (JSONArray) result.get("notice");
                    String[] notice_list = new String[notice.length()];
                    for (int i = 0; i < notice.length(); i++) {
                        JSONObject note = (JSONObject) (notice.get(i));
                        showNote(note.get("spider_name").toString(), note.get("title").toString(), note.get("link").toString());
                        notice_list[i] = note.getString("sn");
                    }
                    String modify_list = new JSONArray(notice_list).toString();
                    if (notice.length() > 0) {
                        // 修改通知状态
                        try {
                            HttpURLConnection p_conn = (HttpURLConnection) url.openConnection();
                            p_conn.setConnectTimeout(6000);
                            p_conn.setReadTimeout(6000);
                            p_conn.setDoOutput(true);
                            p_conn.setDoInput(true);
                            p_conn.setUseCaches(false);
                            p_conn.setRequestProperty("Connection", "Keep-Alive");
                            p_conn.setRequestProperty("Charset", "UTF-8");
                            p_conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                            p_conn.setRequestMethod("POST");
                            byte[] writeBytes = modify_list.getBytes();
                            p_conn.setRequestProperty("Content-Length", String.valueOf(writeBytes.length));
                            OutputStream outWriteStream = p_conn.getOutputStream();
                            outWriteStream.write(modify_list.getBytes());
                            outWriteStream.flush();
                            outWriteStream.close();
                            p_conn.getInputStream();
                        } catch (Exception e) {
                            // 请求出错
                            Log.i(String.valueOf(0), e.toString());
                            e.printStackTrace();
                        }
                    }
                } else {
                    // 读取失败
                    showNote(result.get("title").toString(), result.get("message").toString(), null);
                    application.setLoopStatus(false);
                    stopSelf();
                    return START_NOT_STICKY;
                }
            } else {
                // 状态码非200
                Log.i(String.valueOf(0), returnData);
                showNote(result.get("title").toString(), result.get("message").toString(), null);
            }
        } catch (Exception e) {
            // 请求出错
            Log.i(String.valueOf(0), e.toString());
            showNote("Get notice error", e.getMessage(), null);
            e.printStackTrace();
        }
        // Prepare next loop
        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        long triggerAtTime = SystemClock.elapsedRealtime() + Time;
        Intent intent2 = new Intent(this, AlarmReceiver.class);
        intent2.putExtra("channelName", channelName);
        @SuppressLint("UnspecifiedImmutableFlag") PendingIntent pi = PendingIntent.getBroadcast(this, 0, intent2, 0);
        manager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerAtTime, pi);
        return START_REDELIVER_INTENT;
    }

    public void showNote(String title, String text, String url) {
        Random rand = new Random();
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, "Notice")
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(text)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setDefaults(Notification.DEFAULT_ALL);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.valueOf(url)));
        @SuppressLint("UnspecifiedImmutableFlag") PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        notification.setContentIntent(pendingIntent);
        this.notificationManager.notify(rand.nextInt(9999999), notification.build());
    }

    public static String readStream(InputStream in) throws Exception {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        int len = -1;
        byte[] buffer = new byte[1024]; //1kb
        while ((len = in.read(buffer)) != -1) {
            b.write(buffer, 0, len);
        }
        in.close();
        return b.toString();
    }
}