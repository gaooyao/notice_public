package com.gaoyao.notice;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;


public class MainActivity extends AppCompatActivity {
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // 请求运行后台运行
        requestIgnoreBatteryOptimizations();
        // 允许Service主线程发送http请求
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    // 请求后台运行权限
    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean isIgnoringBatteryOptimizations() {
        boolean isIgnoring = false;
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            isIgnoring = powerManager.isIgnoringBatteryOptimizations(getPackageName());
        }
        return isIgnoring;
    }

    // 请求后台运行权限
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestIgnoreBatteryOptimizations() {
        if (!isIgnoringBatteryOptimizations()) {
            try {
                @SuppressLint("BatteryLife") Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // 用户点击订阅按钮事件
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void subscribe(View view) throws IOException {
        //TODO:连不上服务器状态任意订阅一通道A后，切换到连上服务器状态订阅一正确通道B，此时服务器同时收到来自A和B的查询
        EditText channelInput = (EditText) findViewById(R.id.channel_name);
        Editable channelName = channelInput.getText();
        if (channelName == null || channelName.length() == 0) {
            Toast.makeText(getApplicationContext(), "Please check the channel name.", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(this, AlarmService.class);
        intent.putExtra("channelName", channelName.toString());
        //  Begin loop
        NoticeApplication application = (NoticeApplication) this.getApplication();
        application.setLoopStatus(true);
        startService(intent);
        Toast.makeText(getApplicationContext(), "Subscribing...", Toast.LENGTH_SHORT).show();
    }

    // 用户点击停止按钮事件
    public void stop(View view) throws IOException {
        //  Stop loop
        NoticeApplication application = (NoticeApplication) this.getApplication();
        application.setLoopStatus(false);
        Toast.makeText(getApplicationContext(), "Stopped.", Toast.LENGTH_SHORT).show();
    }

}
