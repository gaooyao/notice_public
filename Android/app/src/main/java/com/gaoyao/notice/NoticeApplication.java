package com.gaoyao.notice;

import android.app.Application;

public class NoticeApplication extends Application {
    private boolean loop_status = false;// 控制查询状态

    public boolean getLoopStatus() {
        return loop_status;
    }

    public void setLoopStatus(boolean loop_status) {
        this.loop_status = loop_status;
    }
}
