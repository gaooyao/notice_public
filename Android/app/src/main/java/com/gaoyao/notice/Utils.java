package com.gaoyao.notice;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class Utils {
    public static String readStream(InputStream in) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int len = -1;
        byte[] buffer = new byte[1024]; //1kb
        while ((len = in.read(buffer)) != -1) {
            baos.write(buffer, 0, len);
        }
        in.close();
        return baos.toString();
    }
}
