import tornado.web


class IndexHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        username = self.get_current_user()
        self.render("index.html", username=username)
