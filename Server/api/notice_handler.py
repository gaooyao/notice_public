import json
import tornado.web
import logging
import config
from database import DBSession, NoticeModel, NoticeSchema, ChannelNameModel, ChannelNameSchema

logger = logging.getLogger(config.LOG_NAME)


class BaseHandler(tornado.web.RequestHandler):
    """
    处理请求基类
    """

    def initialize(self):
        """
        请求创建时连接数据库
        """
        self.db_session = DBSession()

    def on_finish(self):
        """
        请求结束时关闭数据库
        """
        if self.db_session:
            self.db_session.close()

    def data_received(self, chunk):
        pass


class GetNoticeHandler(BaseHandler):
    """
    客户端用，获取通知列表接口
    """

    def get(self, channel_name):
        """
        客户端用，获取通道下的未读通知列表
        :param channel_name: 通道名称
        :return: 未读通知列表
        """
        # 检查通道是否合法
        channel = self.db_session.query(ChannelNameModel).filter_by(channel_name=channel_name).first()
        if not channel:
            self.write({'status': 2, 'title': 'Error', 'message': 'Subscribe fail, this channel is not registered.'})
            self.set_status(200)
            self.set_header("Content-Type", "application/json")
            logger.error('Subscribe fail, channel %s is not registered.' % channel_name)
        else:
            # 获取未读通知列表
            notice = self.db_session.query(NoticeModel).filter_by(channel_name=channel_name, notified=False).all()
            dump_data = NoticeSchema().dump(notice, many=True)
            data = {'status': 1, 'notice': dump_data}
            self.set_status(200)
            self.set_header("Content-Type", "application/json")
            self.write(json.dumps(data))
            logger.info('Get channel %s success.' % channel_name)

    def post(self, channel_name):
        """
        客户端用，更新通知阅读状态接口
        """
        # 查询channel是否存在
        channel = self.db_session.query(ChannelNameModel).filter_by(channel_name=channel_name).first()
        if not channel:
            self.write(
                {'status': 2, 'title': 'Error',
                 'message': 'Update status fail, channel %s is not registered.' % channel_name})
            self.set_status(200)
            self.set_header("Content-Type", "application/json")
            logger.error('Update notice fail, channel %s is not registered.' % channel)
            return
        # 读取要更新的列表
        try:
            data = json.loads(self.request.body.decode('utf-8'))
        except Exception as e:
            self.write({'status': 2, 'title': 'Error', 'message': 'Update notice fail.'})
            self.set_status(200)
            self.set_header("Content-Type", "application/json")
            logger.error('Update notice fail, %s.' % e)
            return
        # 更新数据库
        notice_list = self.db_session.query(NoticeModel).filter(NoticeModel.sn.in_(data)).all()
        for notice in notice_list:
            notice.notified = True
            self.db_session.add(notice)
        try:
            self.db_session.commit()
        except Exception as e:
            logger.error('Update notice fail, %s' % e)
        # 返回
        self.write({'status': 1, 'title': 'Success', 'message': 'Update notice success.'})
        self.set_status(200)
        self.set_header("Content-Type", "application/json")
        logger.info('Update notice %s success.' % json.dumps(data))


class QueryNoticeHandler(BaseHandler):
    """
    查询通知是否已存在接口
    """

    def post(self):
        """
        爬虫端用，查询若干通知是否存在，传入通知序列号列表，返回每个序列号是否已存在
        TODO:此处存在性能瓶颈，考虑序列号存入redis，加快查询速度
        """
        # 读取要查询的通知列表
        try:
            data = json.loads(self.request.body)
        except Exception as e:
            self.write({'status': 3, 'title': 'Error', 'message': 'Query notice fail, notice list is bad.'})
            self.set_status(400)
            self.set_header("Content-Type", "application/json")
            logger.error('Query notice %s fail, notice list is bad.' % e)
            return
        # 查询通知是否存在
        notice = self.db_session.query(NoticeModel.sn).filter(NoticeModel.sn.in_(data)).all()
        notice = [b for a in notice for b in a]
        return_data = {'already_exists': [], 'not_exists': []}
        for sn in data:
            if sn in notice:
                return_data['already_exists'].append(sn)
            else:
                return_data['not_exists'].append(sn)
        # 返回数据
        self.write({'status': 1, 'data': return_data})
        self.set_status(200)
        self.set_header("Content-Type", "application/json")
        logger.info('Query notice success.')


class AddNoticeHandler(BaseHandler):
    """
    增加通知接口
    """

    def post(self, channel_name):
        """
        爬虫端，创建新通知
        :param channel_name: 通知所属通道名称
        """
        # 查询channel是否存在
        channel = self.db_session.query(ChannelNameModel).filter_by(channel_name=channel_name).first()
        if not channel:
            self.write(
                {'status': 4, 'title': 'Error',
                 'message': 'Add notice fail, channel %s is not registered.' % channel_name})
            self.set_status(400)
            self.set_header("Content-Type", "application/json")
            logger.error('Add notice fail, channel %s is not registered.' % channel_name)
            return
        # 读取要更新的列表
        try:
            data = json.loads(self.request.body)
        except Exception as e:
            self.write({'status': 4, 'title': 'Error', 'message': 'Add notice fail.'})
            self.set_status(400)
            self.set_header("Content-Type", "application/json")
            logger.error('Add notice fail, notice list is bad, %s' % e)
            return
        # 更新数据库
        return_data = {'success': [], 'fail': []}
        sn_list = [n['sn'] for n in data]
        exist_notice = self.db_session.query(NoticeModel.sn).filter(NoticeModel.sn.in_(sn_list)).all()
        exist_notice = [b for a in exist_notice for b in a]
        for notice in data:
            # 只添加未在数据库中的通知
            if notice['sn'] not in exist_notice:
                try:
                    schema = NoticeSchema()
                    new_notice = schema.load(notice, session=self.db_session)
                    self.db_session.add(new_notice)
                    self.db_session.commit()
                    return_data['success'].append(notice)
                except Exception as e:
                    return_data['fail'].append({'reason': "" + str(e), 'notice': notice})
            else:
                return_data['fail'].append({'reason': 'Notice already exist.', 'notice': notice})
        # 返回
        self.write({'status': 1, 'title': 'Success', 'message': return_data})
        self.set_status(200)
        self.set_header("Content-Type", "application/json")
        logger.info('Add notice %s success.' % json.dumps(return_data))
