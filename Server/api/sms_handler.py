import tornado.web
import logging
import config
import json
import datetime
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest

logger = logging.getLogger(config.LOG_NAME)


class SmsHandler(tornado.web.RequestHandler):
    def post(self):
        # 发送通知短信
        try:
            data = json.loads(self.request.body.decode('utf-8'))
            assert data['key'] == config.ACCESS_KEY
        except Exception as e:
            self.write({'status': 2, 'title': 'Error', 'message': 'Send sms message fail.'})
            self.set_status(200)
            self.set_header("Content-Type", "application/json")
            logger.error('Send sms message fail, %s.' % e)
            return
        response = None
        try:
            """
            阿里云发送
            """
            client = AcsClient(config.ACCESS_KEY_Id, config.ACCESS_SECRET, 'cn-hangzhou')
            request = CommonRequest()
            request.set_accept_format('json')
            request.set_domain('dysmsapi.aliyuncs.com')
            request.set_method('POST')
            request.set_protocol_type('https')
            request.set_action_name('SendSms')
            request.set_version('2017-05-25')
            request.add_query_param('PhoneNumbers', config.ADMIN_PHONE)
            request.add_query_param('SignName', config.SMS_SIGN_NAME)
            request.add_query_param('TemplateCode', config.SMS_TEMPLATE_CODE)
            param = json.dumps(
                {'e_name': data['message'], 'e_time': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')})
            request.add_query_param('TemplateParam', param)
            response = client.do_action_with_exception(request)
            assert 'OK' in json.loads(response)['Message']
            self.write({'status': 0, 'title': 'Success', 'message': 'Send sms message success.'})
            self.set_status(200)
            self.set_header("Content-Type", "application/json")
            logger.info('Send sms message success, %s.' % data['message'])
        except Exception as e:
            self.write({'status': 2, 'title': 'Error', 'message': 'Send sms message fail.'})
            self.set_status(200)
            self.set_header("Content-Type", "application/json")
            logger.error('Send sms message fail, %s, %s, %s.' % (data['message'], response, e))
