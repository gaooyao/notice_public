REST_PORT = 59090  # 服务器监听端口
LOG_NAME = 'Notice'
LOG_PATH = 'log/main.log'
LOG_FORMAT = '%(asctime)s %(levelname)s %(name)s %(filename)s %(funcName)s(), %(message)s'
SQLITE_DB_PATH = 'sqlite:///data/notice.db'  # 数据库地址

ADMIN_PHONE = '管理员手机号'
ACCESS_KEY = '阿里云账户的ACCESS_KEY'

ACCESS_KEY_Id = '阿里云账户的ACCESS_KEY_Id'
ACCESS_SECRET = '阿里云账户的ACCESS_SECRET'
SMS_SIGN_NAME = '阿里云短信服务的签名名称'
SMS_TEMPLATE_CODE = '阿里云短信服务的模板代码'
