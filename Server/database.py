import datetime
from sqlalchemy import Column
from sqlalchemy import Integer, String, ForeignKey, Float, Boolean, DateTime
from sqlalchemy.orm import relationship
from marshmallow_sqlalchemy import SQLAlchemySchema
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

import config

Base = declarative_base()
engine = create_engine(config.SQLITE_DB_PATH)
DBSession = sessionmaker(bind=engine)


class NoticeModel(Base):
    """
    存储通知表
    """
    __tablename__ = 'basic_notice'
    sn = Column(String(32), primary_key=True, unique=True, nullable=False)  # 此条通知的序列号
    spider_id = Column(Integer, nullable=False)  # 生成此通知的爬虫ID
    spider_name = Column(String(32), nullable=False)  # 生成此通知的爬虫名称
    title = Column(String(128), nullable=False)  # 此通知的标题
    link = Column(String(128), nullable=True)  # 点击通知后要跳转的链接
    create_time = Column(String(32), default=datetime.datetime.now, nullable=False)  # 通知创建时间
    channel_name = Column(String(32), nullable=False)  # 此通知要被发往的通道名称
    notified = Column(Boolean, default=False)  # 此通知是否已被阅读

    def __repr__(self):
        return "<NoticeModel(sn='%s',spider_id='%d',spider_name='%s',title='%s',link='%s'," \
               "create_time='%s',channel_name='%s',notified='%s',>" % (
                   self.sn, self.spider_id, self.spider_name, self.title,
                   self.link, self.create_time, self.channel_name, self.notified)


class NoticeSchema(SQLAlchemySchema):
    class Meta:
        model = NoticeModel
        load_instance = True
        fields = ('sn', 'spider_id', 'spider_name', 'title', 'link', 'channel_name')


class ChannelNameModel(Base):
    """
    此表存储所有通道名称，只允许订阅此表中存在的通道
    """
    __tablename__ = 'channel_name'
    id = Column(Integer, primary_key=True)  # 通道ID
    channel_name = Column(String(32), nullable=False)  # 通道名称

    def __repr__(self):
        return "<ChannelNameModel(id='%d',channel_name='%s'>" % (self.id, self.channel_name)


class ChannelNameSchema(SQLAlchemySchema):
    class Meta:
        model = ChannelNameModel
        fields = ('id', 'channel_name')
