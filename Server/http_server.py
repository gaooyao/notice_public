import logging
import threading
import tornado.ioloop
import tornado.web

import config
import asyncio

import api.index
import api.notice_handler
import api.sms_handler

logger = logging.getLogger(config.LOG_NAME)


class RestServer(threading.Thread):
    """
    The REST server main thread.
    """

    def __init__(self):
        threading.Thread.__init__(self)
        self.setDaemon(True)

    def run(self):
        settings = {
            "template_path": "static",
            "session_timeout": 60,
            "static_path": "static",
            "autoreload": True,
            "xsrf_cookies": False,
            "debug": False,
        }
        asyncio.set_event_loop(asyncio.new_event_loop())
        application = tornado.web.Application([
            (r'/', api.index.IndexHandler),
            (r'/api/get_notice/(?P<channel_name>[\w]+)$', api.notice_handler.GetNoticeHandler),
            (r'/api/query_notice$', api.notice_handler.QueryNoticeHandler),
            (r'/api/add_notice/(?P<channel_name>[\w]+)$', api.notice_handler.AddNoticeHandler),
            (r'/api/send_sms$', api.sms_handler.SmsHandler),
        ], **settings)
        application.listen(config.REST_PORT)
        tornado.ioloop.IOLoop.current().start()
