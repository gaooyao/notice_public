import time
import logging

import config
from database import Base, engine
from http_server import RestServer

if __name__ == '__main__':
    # Init Log
    logging.basicConfig(level=logging.DEBUG, format=config.LOG_FORMAT, filename=config.LOG_PATH, filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(logging.Formatter(config.LOG_FORMAT))
    logger = logging.getLogger(config.LOG_NAME)
    logger.addHandler(console)
    logging.getLogger("tornado.access").addHandler(console)
    logging.getLogger("tornado.application").addHandler(console)
    logging.getLogger("tornado.general").addHandler(console)
    logger.info('Init Log Finished.')
    # Init Db
    logger.info('Init DB ...')
    Base.metadata.create_all(engine)
    logger.info('Init DB Finished.')
    # Init Server
    rest_server = RestServer()
    rest_server.start()
    logger.info('Http Server Started.')
    while True:
        time.sleep(1)
