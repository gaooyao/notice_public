import os
import json
import logging
import threading
import time
import httpx
import config
import utils


class BaseSpider(threading.Thread):
    """
    爬虫基类，创建一线程对内容进行爬取
    """

    def __init__(self, spider_id, name, channel_name):
        """
        爬虫初始化
        :param spider_id: 该爬虫ID
        :param name: 该爬虫名称
        :param channel_name: 该爬虫创建的通知所属的通道名称
        """
        super().__init__()
        self.id = spider_id  # 爬虫ID
        self.name = name  # 爬虫名称
        self.channel_name = channel_name  # 爬虫所属通道名称
        self.server = config.SERVER_HOST  # 爬虫提交通知的服务器地址
        self.get_api = config.GET_API  # 从服务器获取通知列表的API
        self.query_api = config.QUERY_API  # 从服务器查询通知是否存在API
        self.add_api = config.ADD_API  # 向服务器新增通知的API
        self.wait_time = config.WAIT_TIME  # 爬虫轮询时等待时间
        self.query_number = config.QUERY_NUMBER  # 每次要爬取的条目数量
        self.session = httpx.Client(timeout=30)  # 爬虫的HTTP Client，用于保持状态
        # Init Log
        self.logger = logging.getLogger(self.name)
        console = logging.FileHandler(config.LOG_PATH + self.name + '.log')
        console.setLevel(logging.DEBUG)
        console.setFormatter(logging.Formatter(config.LOG_FORMAT))
        self.logger.addHandler(console)
        self.info_log('Init Log Finished.')
        # Load session status
        self.load_session()

    def error_log(self, message):
        """
        记录错误日志
        :param message: 日志内容
        """
        self.logger.error('%s %s %s %s' % (self.channel_name, self.id, self.name, message))

    def info_log(self, message):
        """
        记录信息日志
        :param message: 日志内容
        """
        self.logger.info('%s %s %s %s' % (self.channel_name, self.id, self.name, message))

    def save_session(self):
        """
        保存session状态减少登录次数
        :return:
        """
        with open('data/' + str(self.id) + '.session', 'w', encoding='utf-8') as f:
            data = {'headers': dict(self.session.headers), 'cookies': dict(self.session.cookies)}
            f.write(json.dumps(data))
        self.info_log('Save session success.')

    def load_session(self):
        """
        加载保存的session状态
        :return:
        """
        path = 'data/' + str(self.id) + '.session'
        if os.path.isfile(path):
            with open(path, 'r', encoding='utf-8') as f:
                data = json.loads(f.read())
                self.session.headers.update(data['headers'])
                for key, value in data['cookies'].items():
                    self.session.cookies.set(key, value)
        self.info_log('Load session success.')

    def get(self, url, params=None):
        """
        通过session发送get请求
        :param url: 请求地址
        :param params: 请求参数
        """
        r = self.session.get(url, params=params)
        return str(r.text)

    def post(self, url, params=None, payload=None):
        """
        通过session发送post请求
        :param url: 请求地址
        :param params: 请求参数
        :param payload: 请求body内容
        """
        r = self.session.post(url, params=params, data=payload)
        return str(r.text)

    def put(self, url, params=None, payload=None):
        """
        通过session发送put请求
        :param url: 请求地址
        :param params: 请求参数
        :param payload: 请求body内容
        """
        r = self.session.put(url, params=params, data=payload)
        return str(r.text)

    def run(self):
        """
        爬虫主循环，每间隔self.wait_time后进行爬取操作
        """
        while True:
            data = self.get_data()
            self.post_data(data)
            self.save_session()
            time.sleep(self.wait_time)

    def get_data(self):
        """
        生成的爬虫须自定义的函数，获取关注的内容列表
        :return: 内容列表数字，每条目包含标题和跳转连接，如[{'title':'重大新闻：我国将****','link':'https://www.sina.com'}]
        """
        return []

    def post_data(self, data):
        """
        根据爬取到的数据，向服务器增加新通知
        :param data: get_data函数返回的列表
        """
        if not len(data):
            return 0
        # 整理数据
        for i in data:
            i['channel_name'] = self.channel_name
            i['spider_id'] = self.id
            i['spider_name'] = self.name
        # 计算通知的序列号
        notice_list = {}
        for d in data:
            notice_list[utils.get_sn(d)] = d
        # 查询通知是否已在数据库中
        try:
            query = self.post(self.server + self.query_api, None, json.dumps(list(notice_list.keys())))
            query = json.loads(query)
            assert 'not_exists' in query['data']
        except Exception as e:
            self.error_log('Query notice fail, %s' % e)
            return 0
        # 生成要添加的新通知列表
        add_list = []
        for d in query['data']['not_exists']:
            notice_list[d]['sn'] = d
            add_list.append(notice_list[d])
        # 向服务器提交新通知
        if len(add_list):
            try:
                r = self.post(self.server + self.add_api + self.channel_name, None, json.dumps(add_list))
                r = json.loads(r)
                assert r['status'] == 1
                self.info_log('Add notice success, %s' % r['message']['success'])
                return 1
            except Exception as e:
                self.error_log('Add notice fail, %s' % e)
                return 0
