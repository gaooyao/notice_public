SERVER_HOST = 'Server地址，如http://127.0.0.1:8000'  # 服务器地址
WAIT_TIME = 60 * 10  # 爬虫轮询等待秒数
QUERY_NUMBER = 20  # 爬取时最大爬取数量
# log配置
LOG_NAME = 'Spider'
LOG_PATH = 'log/'
LOG_FORMAT = '%(asctime)s %(levelname)s %(name)s %(filename)s %(funcName)s(), %(message)s'

GET_API = '/api/get_notice/'  # 读取通知API
QUERY_API = '/api/query_notice'  # 查询通知存在API
ADD_API = '/api/add_notice/'  # 新增通知API
SMS_API = '/api/send_sms'  # 发送短信通知API

ACCESS_KEY = '阿里云ACCESS_KEY'
