import time
import atexit
import logging
import config
from spiders.byr_bbs_computer import ByrSecondHandSpider
from spiders.byr_bbs_ad import ByrAdvertisingSpider
from spiders.byr_pt import ByrPtSpider
from spiders.moodle import MoodleSpider

spider_list = []

if __name__ == '__main__':
    # Init Log
    logging.basicConfig(level=logging.DEBUG, format=config.LOG_FORMAT, filename=config.LOG_PATH + 'main.log',
                        filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(logging.Formatter(config.LOG_FORMAT))
    logger = logging.getLogger(config.LOG_NAME)
    logger.addHandler(console)
    logger.info('Init Log Finished.')
    # 数码交易爬虫
    byr_secondhand = ByrSecondHandSpider(1, 'byr_bbs_secondhand', 'gaoyao')
    byr_secondhand.start()
    spider_list.append(byr_secondhand)

    # 跳蚤市场爬虫
    byr_ad = ByrAdvertisingSpider(3, 'byr_bbs_ad', 'gaoyao')
    byr_ad.start()
    spider_list.append(byr_ad)

    # PT网站爬虫
    # byr_ad = ByrPtSpider(5, 'byr_pt', 'gaoyao')
    # byr_ad.start()
    # spider_list.append(byr_ad)

    while True:
        time.sleep(5)
