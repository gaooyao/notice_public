import time
from bs4 import BeautifulSoup as bf
from base_spider import BaseSpider


class ByrSecondHandSpider(BaseSpider):
    """
    北邮人论坛数码交易市场爬虫
    """

    def __init__(self, spider_id, name, channel_name):
        super().__init__(spider_id, name, channel_name)
        self.user = ''  # 论坛登录账号
        self.passwd = ''  # 论坛登录密码

    def check_login(self):
        """
        检查是否已经登陆论坛
        """
        try:
            # 如果能获取到默认文章列表则表示session已处于登录状态
            html = self.get('https://bbs.byr.cn/default?_uid=' + self.user)
            html = bf(html, features='lxml').findAll(class_='name')
            assert len(html) > 0
        except Exception as err:
            # 未获取到默认文章列表则需要登录论坛
            try:
                self.session.headers['Origin'] = 'https://bbs.byr.cn'
                self.get('https://bbs.byr.cn/index')  # 进入论坛主页
                time.sleep(1)
                self.session.headers['Referer'] = 'https://bbs.byr.cn/'
                self.session.headers['Accept'] = 'application/json, text/javascript, */*; q=0.01'
                self.session.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
                self.session.headers['X-requested-with'] = 'XMLHttpRequest'
                self.session.cookies.set('login-user', self.user)
                self.session.cookies.set('left-index', '0001000000')
                self.post('https://bbs.byr.cn/user/ajax_login.json', payload={
                    'id': self.user,
                    'passwd': self.passwd,
                    'mode': "0",
                    'CookieDate': "0"})  # 发送登录请求
                time.sleep(1)
            except Exception as e:
                self.error_log('Login in fail, %s.' % e)

    def get_data(self):
        """
        获取在售物品列表
        :return: 在售物品列表
        """
        try:
            # 检查登陆状态
            self.check_login()
            # 获取商品列表
            html = self.get('https://bbs.byr.cn/board/ComputerTrade?_uid=' + self.user)
            html = bf(html, features='lxml').findAll(class_='title_9')
            html = html[1:]
            # 整理数据
            data = []
            for i in html:
                if i.parent.get('class') is None:
                    link = i.find('a')
                    data.append({'title': link.text, 'link': 'https://bbs.byr.cn' + link.get('href')})
            self.info_log('Query new notice success.')
            return data
        except Exception as e:
            self.error_log('Query new notice fail, %s.' % e)
            return []
