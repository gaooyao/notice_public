import time
import ddddocr
from bs4 import BeautifulSoup as bf
from base_spider import BaseSpider


class ByrPtSpider(BaseSpider):
    """
    北邮人PT网站免费种子爬虫
    """

    def __init__(self, spider_id, name, channel_name):
        super().__init__(spider_id, name, channel_name)
        self.user = ''  # 论坛登录账号
        self.passwd = ''  # 论坛登录密码
        self.wait_time = 60 * 10
        self.query_number = 10
        self.session.headers[
            'User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36'

    def login(self):
        try:
            self.session.headers['Origin'] = 'https://byr.pt'
            html = self.get('https://byr.pt/login.php')  # 进入登陆页面
            time.sleep(1)
            src = 'https://byr.pt/' + bf(html, features='lxml').findAll(alt='CAPTCHA')[0].get('src')
            response = self.session.get(src)
            ocr = ddddocr.DdddOcr()
            v_code = ocr.classification(response.content)
            if len(v_code) != 6:
                time.sleep(1)
                return 0
            time.sleep(1)
            self.session.headers['Referer'] = 'https://byr.pt/login.php'
            self.session.headers[
                'Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'
            self.session.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
            html = self.post('https://byr.pt/takelogin.php', payload={
                'username': self.user,
                'password': self.passwd,
                'imagestring': v_code,
                'imagehash': src[-32:]})  # 发送登录请求
            self.info_log(bf(html, features='lxml').findAll(class_='embedded')[0].get_text())
            time.sleep(1)
            return 1
        except Exception as e:
            self.error_log('Login in fail, %s.' % e)
            time.sleep(1)
            return 0

    def check_login(self):
        """
        检查是否已经登陆论坛
        """
        try:
            # 如果能获取到默认文章列表则表示session已处于登录状态
            html = self.get('https://byr.pt/login.php')
            assert '你已经登录！' in html
            return 1
        except Exception as err:
            return 0

    def get_data(self):
        """
        获取帖子列表
        """
        try:
            # 检查登陆状态
            while not self.check_login():
                time.sleep(10)
                self.login()
            # 获取列表
            html = self.get('https://byr.pt/torrents.php?spstate=2')
            html = bf(html, features='lxml').findAll('a')
            # 整理数据
            data = []
            for i in html:
                if i.get('title') and i.get('href') and 'details.php?id=' in i.get('href'):
                    data.append(i)
            html = data[:self.query_number]
            data = []
            for i in html:
                data.append({'title': i.get('title'), 'link': 'https://byr.pt/' + i.get('href')})
            self.info_log('Query new notice success.')
            return data
        except Exception as e:
            self.error_log('Query new notice fail, %s.' % e)
            return []
