import hashlib


def get_sn(obj):
    return hashlib.md5(str(obj).encode(encoding='UTF-8')).hexdigest()
